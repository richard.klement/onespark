module.exports = class MarsRover {

    constructor(name, positionX, positionY, direction, maxPlateauX, maxPlateauY) {
        this.name = name;
        this.directions = ["N", "E", "S", "W"];
        this.instructions = null;
        this.position = [positionX, positionY];
        this.direction = direction;
        this.maxPlateauX = maxPlateauX;
        this.maxPlateauY = maxPlateauY;
    }

    setInstructions(instruct) {
        this.instructions = instruct;
    }

    mapPlateau() {
        for (var i = 0; i < this.instructions.length; i++) {
            var command = this.instructions[i];

            if (command == "M") {

                this.moveForward(command);

            } else if (command == "L" || command == "R") {
                this.turn(command);

            }
        }
        console.log( this.name + ": " + this.position + " direction:" + this.direction);
    }

    turn(command) {

        var cardinalIndex = this.directions.indexOf(this.direction);

        if (command == "L") {
            cardinalIndex = (cardinalIndex + 4 - 1) % 4;
        } else if (command == "R") {
            cardinalIndex = (cardinalIndex + 1) % 4;
        }
        this.direction = this.directions[cardinalIndex];
        console.log('Turning to ' + this.direction);
    }

    moveForward(command) {

        if (command == "M") {
            var coordinateX = this.position[0];
            var coordinateY = this.position[1];
            switch(this.direction){
                case 'N':
                    if(coordinateY != this.maxPlateauY){
                    coordinateY++;}
                    else{
                    console.log(this.name + " is at the top of the plateau and can't go further");}
                    break;
                case 'E':
                    if(coordinateX != this.maxPlateauX){
                    coordinateX++;}
                    else{
                        console.log(this.name + " is at the right edge of the plateau and can't go further");}
                    break;
                case 'S':
                    if(coordinateY != 0){
                    coordinateY--;}
                    else{
                        console.log(this.name + " is at the bottom of the plateau and can't go further");}
                    break;
                case 'W':
                    if(coordinateX != 0){
                    coordinateX--;}
                    else{
                        console.log(this.name + " is at the left edge of the plateau and can't go further");}
                    break;
            }
            console.log('direction: ' + this.direction + ' location: ' + '[' + coordinateX + ']' + '[' + coordinateY + ']')

        } else {
            alert("Incorrect command");
        }
        var newPosition = [coordinateX, coordinateY];
        this.position = newPosition;
        console.log(this.position);
    }
}