var fs = require('fs');
var MarsRover = require('./components/rover.js'); // class for the rover
var Mars = require('./components/mars.js'); // class for the plateau

var inputs = fs.readFileSync(process.argv[2], 'utf8').toString();
var commands = inputs.split("\n");

var platSize = getPlateauSize(commands[0]);

var curiosityPos = getRoverPosition(commands[1]);

var georgePos = getRoverPosition(commands[3]);

var curiosityDir = curiosityPos[2].replace('\r', '');

var georgeDir = georgePos[2].replace('\r', '');




let curiosity = new MarsRover("curiosity", curiosityPos[0], curiosityPos[1], curiosityDir, platSize[0], platSize[1]);

curiosity.setInstructions(commands[2]);

let george = new MarsRover("george", georgePos[0], georgePos[1], georgeDir, platSize[0], platSize[1]);

george.setInstructions(commands[4]);

let mars = new Mars(platSize[0],platSize[1])

mars.addRover(curiosity);
mars.addRover(george);

 mars.explore();

function getPlateauSize(command) {
 var gridTextArr = command.split(" ");
 return [parseInt(gridTextArr[0]), parseInt(gridTextArr[1])];
}

function getRoverPosition(command) {
 var sP = command.split(" ");
 return [parseInt(sP[0]), parseInt(sP[1]), (sP[2])];
}